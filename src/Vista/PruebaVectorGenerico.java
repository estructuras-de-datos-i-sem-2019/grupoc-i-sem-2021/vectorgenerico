/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Punto;
import ufps.util.colecciones_seed.VectorGenerico;

/**
 *
 * @author madar
 */
public class PruebaVectorGenerico {
    
    
    public static void main(String[] args) {
        
        try{
            
                VectorGenerico<Integer> v1=new VectorGenerico(3);
                VectorGenerico<Punto> v2=new VectorGenerico(2);
                v1.add(1333); //--> i=0
                v1.add(13); //-->i=1
                v1.add(-133); //--> i=2
                v1.sort();
               // v1.add(4); // --> :( 
                System.out.println(v1.toString());
                
                //Creando puntos:
                v2.add(new Punto(3,4));
                v2.add(new Punto(3,3));
                v2.sort();
                System.out.println(v2.toString());
                
        }catch(Exception e)
        {
            System.err.println(e.getMessage());
        }
        //V1={3,13,133}
        
    }
}
